Webster's Unabridged Dictionary - Comandline tool

  by Adam Byers (al14s)


Files:
	673.txt              HTML version of the dictionary from gutenburg.org
	1828_parse.py        script used to parse the raw text - 673.txt
	1828_dict.shelvedb   shelve database containing said dictionary
	def.py               used to query the dictionary database



*Recommend creating a shell wrapper that pipes the output into 'less'.
