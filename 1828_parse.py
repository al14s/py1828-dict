#!/usr/bin/env python3
import re, shelve, os

os.system('rm -rf bad.txt')
dat = open('673.txt', errors="ignore").read().split('<h1>')[1:]
scriptpath = os.path.dirname(os.path.realpath(__file__))
os.system('rm -rf ' + scriptpath + '/1828_dict.shelvedb')
d = shelve.open(scriptpath + '/1828_dict.shelvedb')

for l in dat:
    for x, y in [('8a', 'e'), ('8b', 'i'), ('80', 'c'), ('82', 'e'),
                 ('83', 'i'), ('85', 'o'), ('87', 'c'), ('88', 'e'),
                 ('89', 'e'), ('90', 'e'), ('91', 'a'), ('92', 'ae'),
                 ('93', 'o'), ('94', 'o'), ('a4', 'n'), ('d1', 'e'),
                 ('d2', 'oe'), ('81', 'u'), ('ee', 'a'), ('8c', 'i'),
                 ('96', 'u'), ('97', 'u'), ('84', 'a'), ('c6', 'i'),
                 ('b5', 'i')]:
        l = l.replace("\\'" + x, y)

    w, x = l.split('</h1>')
    w = re.sub('<[^>]+>', '', w)
    w = w.strip('-')
    w = w.replace("a.", '')
    w = w.replace("adj.", '')
    w = w.replace("v.", '')
    w = w.replace("n.", '')
    w = w.replace("&or;", '')
    w = w.replace(",", '')
    w = w.strip()

    #  These are HTML characters that didn't convert.
    if "\\'" in w: open('bad.txt', 'a').write(w + '\t' + x + '\n')

    o = x  # capture the original x
    x = x.replace('\r', '')
    for a in re.findall("""<blockquote>.*?\<\/blockquote>""", x, re.DOTALL):
        x = x.replace(a, '\t' + a.replace('\n<i>', '  - ')
        x = x.replace('\n', '\n\t'))

    x = x.replace('\n\n<i>', '  - ')
    x = x.replace('<ety>', '\n\n  ')
    x = x.replace("&hand;", '  ')
    x = re.sub('<[^>]+>', '', x)
    x = x.rstrip('>').lstrip('\n')
    x = re.sub('Page [\d]+', '', x)

    if x == '': print(o)  # Make sure we haven't edited x out of existence...
    if not w.lower() in d: d[w.lower()] = (w, x)
    else:
        x1 = d[w.lower()][1]
        d[w.lower()] = (w, x1 + '\n ' + x)

d.close()
