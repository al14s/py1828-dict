#!/usr/bin/env python3
import shelve, os, sys

usage = "\n\n"+' '*12+"Webster's 1828 Dictionary\n\n"+' '*16+"Exact:   " + \
"def <word>\n          Starts With:   def -s <word>\n"+' '*13+"Contains:" + \
"   def -c <word>\n\n"

if len(sys.argv) == 1: print(usage)
else:
    scriptpath = os.path.dirname( os.path.realpath(__file__) )
    d = shelve.open(scriptpath + '/1828_dict.shelvedb')
    t = sys.argv[1]
    q = sys.argv[-1].lower().strip()
    for b in d.keys():
        if ((t == q) and (q == b)) or \
            ((t == '-c') and (q in b)) or \
            ((t == '-s') and b.startswith(q)):
            print( '\n\n\033[1m' + b + '\033[0m\n\n ' + d[b][1] )

    d.close()
